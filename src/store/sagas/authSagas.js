import {put, call, all, takeEvery} from 'redux-saga/effects';
import * as actions from '../actions';
import firebase from '../../services/Firebase';
import {CONSTANTS} from '../../constants/actions';
import * as ROUTES from '../../constants/routes';
import { push } from 'connected-react-router'

function* setAuthSaga() {
  try {
    const result = yield new Promise((resolve) => {
      firebase.auth.onAuthStateChanged((user) => {
        if (user) {
          resolve(user);
        }
      });
    });
    yield put(actions.authSuccess(result));
  }
  catch(error){
    yield put(actions.authFailed(error));
  }
}

function* signOutSaga() {
  yield call(() => firebase.auth.signOut());
}

function* signInSaga(action){
  const { email, password } = action.payload.auth;
  try {
    yield call(() => firebase.auth.signInWithEmailAndPassword(email, password));
    const result = yield new Promise((resolve) => {
      firebase.auth.onAuthStateChanged((user) => {
        if (user) {
          resolve(user);
        }
      });
    });
    yield put(actions.authSuccess(result));
    yield put(push(ROUTES.HOME));
  } catch (error) {
    yield put(actions.signInFailed(error));
  }
}

function* signUpSaga(action){
  const { username, email, passwordOne} = action.payload.auth;
  try {
    const authUser = yield call(() => firebase.auth.createUserWithEmailAndPassword(email, passwordOne));
    yield call(() => firebase
      .user(authUser.user.uid)
      .set({
        username,
        email,
        isAdmin: 0
      })
    );
    const result = yield new Promise((resolve) => {
      firebase.auth.onAuthStateChanged((user) => {
        if (user) {
          resolve(user);
        }
      });
    });
    yield put(actions.authSuccess(result));
  } catch (error) {
    yield put(actions.signUpFailed(error));
  }
}
function* passwordForgetSaga(action){
  try {
    yield call(() => firebase.auth.sendPasswordResetEmail(action.payload.auth.email));
    yield put(actions.passwordForgetSuccess());
  } catch (error) {
    yield put(actions.passwordFailed(error));
  }
}
function* passwordChangeSaga(action){
  try {
    yield call(() => firebase.auth.currentUser.updatePassword(action.payload.password));
    yield put(actions.passwordChangeSuccess());
  } catch (error) {
    yield put(actions.passwordFailed(error));    
  }
}

export default function* watchAuth() {
  yield all([
    yield takeEvery(CONSTANTS.SET_AUTH, setAuthSaga),
    yield takeEvery(CONSTANTS.SIGN_OUT, signOutSaga),
    yield takeEvery(CONSTANTS.SIGN_IN, signInSaga),
    yield takeEvery(CONSTANTS.SIGN_UP, signUpSaga),
    yield takeEvery(CONSTANTS.PASSWORD_FORGET, passwordForgetSaga),
    yield takeEvery(CONSTANTS.PASSWORD_CHANGE, passwordChangeSaga),
  ])
}