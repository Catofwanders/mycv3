import {call, put, all, takeEvery} from 'redux-saga/effects';
import * as actions from '../actions';
import firebase from '../../services/Firebase';
import {CONSTANTS} from '../../constants/actions';

function* getPostsSaga() {
  try {
    const res = yield call(()=>firebase.posts());
    const result = yield new Promise((resolve) => {
      res.on('value', snapshot => {
        const postsObject = snapshot.val();
        if(postsObject !== null && postsObject !== undefined){
          const postsList = Object.keys(postsObject).map(key => ({
            ...postsObject[key],
            uid: key,
          }));
          resolve(postsList);
        }
      });
    });
    yield put(actions.getPostsSuccess(result));
  } catch (error) {
    yield put(actions.getPostsFailed(error));
  }
}

export default function* watchPosts() {
  yield all([
    yield takeEvery(CONSTANTS.GET_POSTS, getPostsSaga),
  ])
}