import {call, put, all, takeEvery} from 'redux-saga/effects';
import * as actions from '../actions';
import firebase from '../../services/Firebase';
import {CONSTANTS} from '../../constants/actions';

function addPostData(id, title, descr, date) {
  firebase.post(id).set({
    post_title: title,
    post_descr: descr,
    post_date: date
  });
}

function* adminAddPostSaga(action) {
  try {
    const {id, title, descr, date} = action.payload;
    yield call(() => addPostData(id, title, descr, date));
    yield put(actions.adminAddPostSuccess());
    yield put(actions.adminGetPosts());
  } catch (error) {
    yield put(actions.adminAddPostFailed(error));
  }
}

function* adminDeletePostSaga(action) {
  try {
    const {id} = action.payload;
    yield call(()=> firebase.post(id).remove());
    yield put(actions.adminDeletePostSuccess());
    yield put(actions.adminGetPosts());
  } catch (error) {
    yield put(actions.adminDeletePostFailed(error));
  }
}

function* adminEditPostSaga(action) {
  try {
    const {id, title, descr} = action.payload;
    yield call(()=> firebase.post(id).update({
      post_title: title,
      post_descr: descr
    }));
    yield put(actions.adminEditPostSuccess());
    yield put(actions.adminGetPosts());
  } catch (error) {
    yield put(actions.adminEditPostFailed(error));
  }
}

function* adminGetPostsSaga() {
  try {
    const res = yield call(()=>firebase.posts());
    const result = yield new Promise((resolve) => {
      res.on('value', snapshot => {
        const postsObject = snapshot.val();
        if(postsObject !== null && postsObject !== undefined){
          const postsList = Object.keys(postsObject).map(key => ({
            ...postsObject[key],
            uid: key,
          }));
          resolve(postsList);
        }
      });
    });
    yield put(actions.adminGetPostsSuccess(result));
  } catch (error) {
    yield put(actions.adminGetPostsFailed(error));
  }
}

function* adminGetUsersSaga() {
  try {
    const res = yield call(()=>firebase.users());
    const result = yield new Promise((resolve) => {
      res.on('value', snapshot => {
        const usersObject = snapshot.val();
        if(usersObject !== null && usersObject !== undefined){
          const usersList = Object.keys(usersObject).map(key => ({
            ...usersObject[key],
            uid: key,
          }));
          resolve(usersList);
        }
    });
  });
  yield put(actions.adminGetUsersSuccess(result));
  } catch (error) {
    yield put(actions.adminGetUsersFailed(error));
  }
  
}

export default function* watchAdmin() {
  yield all([
    yield takeEvery(CONSTANTS.ADMIN_ADD_POST, adminAddPostSaga),
    yield takeEvery(CONSTANTS.ADMIN_DELETE_POST, adminDeletePostSaga),
    yield takeEvery(CONSTANTS.ADMIN_EDIT_POST, adminEditPostSaga),
    yield takeEvery(CONSTANTS.ADMIN_GET_POSTS, adminGetPostsSaga),
    yield takeEvery(CONSTANTS.ADMIN_GET_USERS, adminGetUsersSaga),
  ])
}