import {all,fork} from 'redux-saga/effects';
import adminSagas from './adminSagas';
import authSagas from './authSagas';
import postsSagas from './postsSagas';

export default function* rootSaga() {
	yield all([
		fork(adminSagas),
		fork(authSagas),
		fork(postsSagas),
	]);
}
