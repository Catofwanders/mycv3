import adminReducer from './adminReducer';
import authReducer from './authReducer';
import postReducer from './postReducer';

const reducers = {
  adminReducer,
  authReducer,
  postReducer
};

export default reducers;