import {CONSTANTS} from '../../constants/actions';
import Immutable from 'seamless-immutable';

const initialState = {
  posts: []
}

const getPosts = (state, action) => {
  return state;
}
const getPostsSuccess = (state, action) => {
  return Immutable.set(state, 'posts', action.payload.posts);
}
const getPostsFailed = (state, action) => {
  return Immutable.set(state, 'error', action.payload.error);
}

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.GET_POSTS: return getPosts(state, action);
    case CONSTANTS.GET_POSTS_SUCCESS: return getPostsSuccess(state, action);
    case CONSTANTS.GET_POSTS_FAILED: return getPostsFailed(state, action);
    default: return state;
  }
};

export default postReducer;