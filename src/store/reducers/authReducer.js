import Immutable from 'seamless-immutable';
import CommonUtils from '../../utils/CommonUtils';
import {CONSTANTS} from '../../constants/actions';

const initialState = Immutable.from({
  auth:{
    email: '',
    loading: false,
    message: '',
    error: ''
  },
});

const setAuth = (state, action) => {
  return state;
}

const authSuccess = (state, action) => {
  const result = CommonUtils.safeMerge(state.auth, action.payload.auth);
  return Immutable.set(state, 'auth', result);
}

const authFailed = (state, action) => {
  return Immutable.set(state, 'error', action.payload.error.message);
}

const signOut = (state, action) => {
  return initialState;
}

const signIn = (state, action) => {
  return state;
}
const signInFailed = (state, action) => {
  let error = {error: action.payload.error.message};
  return Immutable.set(state, 'auth', error);
}
const signUp = (state, action) => {
  return state;
}
const signUpFailed = (state, action) => {
  const result = CommonUtils.safeMerge(state.auth, action.payload.error);
  return Immutable.set(state, 'auth', result);
}
const passwordForget = (state, action) => {
  return state;
}
const passwordFailed = (state, action) => {
  let error = {error: action.payload.error.message};
  return Immutable.set(state, 'auth', error);
}
const passwordForgetSuccess = (state, action) => {
  const result = CommonUtils.safeMerge(state.auth, action.payload.auth);
  return Immutable.set(state, 'auth', result);
}
const passwordChangeSuccess = (state, action) => {
  const result = CommonUtils.safeMerge(state.auth, action.payload.auth);
  return Immutable.set(state, 'auth', result);
}
const passwordChange = (state, action) => {
  return state;
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.SET_AUTH: return setAuth(state, action);
    case CONSTANTS.AUTH_SUCCESS: return authSuccess(state, action);
    case CONSTANTS.AUTH_FAILED: return authFailed(state, action);
    case CONSTANTS.SIGN_OUT: return signOut(state, action);
    case CONSTANTS.SIGN_IN: return signIn(state, action);
    case CONSTANTS.SIGN_IN_FAILED: return signInFailed(state, action);
    case CONSTANTS.SIGN_UP: return signUp(state, action);
    case CONSTANTS.SIGN_UP_FAILED: return signUpFailed(state, action);
    case CONSTANTS.PASSWORD_FORGET: return passwordForget(state, action);
    case CONSTANTS.PASSWORD_FORGET_SUCCESS: return passwordForgetSuccess(state, action);
    case CONSTANTS.PASSWORD_FAILED: return passwordFailed(state, action);
    case CONSTANTS.PASSWORD_CHANGE: return passwordChange(state, action);
    case CONSTANTS.PASSWORD_CHANGE_SUCCESS: return passwordChangeSuccess(state, action);
    default: return state;
  }
}

export default authReducer;