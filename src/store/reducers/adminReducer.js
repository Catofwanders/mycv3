import {CONSTANTS} from '../../constants/actions';
import Immutable from 'seamless-immutable';

const initialState = {
  posts: [],
  users: []
};

const adminAddPost = (state, action) => {
  return state;
}
const adminAddPostSuccess = (state, action) => {
  return state;
}
const adminAddPostFailed = (state, action) => {
  return state;
}
const adminEditPost = (state, action) => {
  return state;
}
const adminEditPostSuccess = (state, action) => {
  return state;
}
const adminEditPostFailed = (state, action) => {
  return state;
}
const adminDeletePost = (state, action) => {
  return state;
}
const adminDeletePostSuccess = (state, action) => {
  return state;
}
const adminDeletePostFailed = (state, action) => {
  return state;
}
const adminGetPosts = (state, action) => {
  return state;
}
const adminGetPostsSuccess = (state, action) => {
  return Immutable.set(state, 'posts', action.payload.posts);
}
const adminGetPostsFailed = (state, action) => {
  return Immutable.set(state, 'error', action.payload.error);
}
const adminGetUsers = (state, action) => {
  return state;
}
const adminGetUsersSuccess = (state, action) => {
  return Immutable.set(state, 'users', action.payload.users);
}
const adminGetUsersFailed = (state, action) => {
  return Immutable.set(state, 'error', action.payload.error);
}

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.ADMIN_ADD_POST: return adminAddPost(state, action);
    case CONSTANTS.ADMIN_ADD_POST_SUCCESS: return adminAddPostSuccess(state, action);
    case CONSTANTS.ADMIN_ADD_POST_FAILED: return adminAddPostFailed(state, action);
    case CONSTANTS.ADMIN_EDIT_POST: return adminEditPost(state, action);
    case CONSTANTS.ADMIN_EDIT_POST_SUCCESS: return adminEditPostSuccess(state, action);
    case CONSTANTS.ADMIN_EDIT_POST_FAILED: return adminEditPostFailed(state, action);
    case CONSTANTS.ADMIN_DELETE_POST: return adminDeletePost(state, action);
    case CONSTANTS.ADMIN_DELETE_POST_SUCCESS: return adminDeletePostSuccess(state, action);
    case CONSTANTS.ADMIN_DELETE_POST_FAILED: return adminDeletePostFailed(state, action);
    case CONSTANTS.ADMIN_GET_POSTS: return adminGetPosts(state, action);
    case CONSTANTS.ADMIN_GET_POSTS_SUCCESS: return adminGetPostsSuccess(state, action);
    case CONSTANTS.ADMIN_GET_POSTS_FAILED: return adminGetPostsFailed(state, action);
    case CONSTANTS.ADMIN_GET_USERS: return adminGetUsers(state, action);
    case CONSTANTS.ADMIN_GET_USERS_SUCCESS: return adminGetUsersSuccess(state, action);
    case CONSTANTS.ADMIN_GET_USERS_FAILED: return adminGetUsersFailed(state, action);
    default: return state;
  }
}

export default adminReducer;