import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import rootSaga from './sagas';
import reducers from './reducers';

const isProduction     = false;
export const history   = createBrowserHistory();
const sagaMiddleware   = createSagaMiddleware();
const routeMiddleware  = routerMiddleware(history);
const middlewares      = [sagaMiddleware, routeMiddleware];

const composeEnhancers = (
	!isProduction
	&& typeof window === 'object'
	&& window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
)
	? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ trace: true, traceLimit: 20 })
	: compose;

const store = createStore(
  combineReducers({
    ...reducers,
    router: connectRouter(history),
  }),
  composeEnhancers(applyMiddleware(...middlewares)),
);

sagaMiddleware.run(rootSaga);

export { 
  store, 
};