import { createSelector } from 'reselect';

const user = ({authReducer}) => authReducer.auth;

export const selectUser  = createSelector(
  [user],
  (user) => user,
);

export const selectIsLoggedIn = createSelector(
  [selectUser],
  (user) => Boolean(user && user.email)
)