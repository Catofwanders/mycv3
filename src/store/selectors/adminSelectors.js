import { createSelector } from 'reselect';

const posts = ({adminReducer}) => adminReducer.posts;
const users = ({adminReducer}) => adminReducer.users;

export const selectPosts  = createSelector(
  [posts],
  (posts) => posts,
);

export const selectUsers = createSelector(
  [users],
  (users) => users
);