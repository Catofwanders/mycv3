import { createSelector } from 'reselect';

const posts = ({postReducer}) => postReducer.posts;
export const selectPosts  = createSelector(
  [posts],
  (posts) => posts,
);