import {CONSTANTS} from '../../constants/actions';

export const getPosts = () => {
  return{
    type: CONSTANTS.GET_POSTS,
  }
}
export const getPostsFailed = (error) => {
  return{
    type: CONSTANTS.GET_POSTS_FAILED,
    payload: error
  }
}
export const getPostsSuccess = (posts) => {
  return{
    type: CONSTANTS.GET_POSTS_SUCCESS,
    payload: {
      posts
    }
  }
}