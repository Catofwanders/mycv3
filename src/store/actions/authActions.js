import {CONSTANTS} from '../../constants/actions';

export const setAuth = (loading) => {
  return {
    type: CONSTANTS.SET_AUTH,
    payload: loading
  }
}
export const authSuccess = (auth) => {
  return {
    type: CONSTANTS.AUTH_SUCCESS,
    payload: {
      auth
    }
  }
}
export const authFailed = (error) => {
  return {
    type: CONSTANTS.AUTH_FAILED,
    payload: {
      error
    }
  }
}
export const signOut = () => {
  return {
    type: CONSTANTS.SIGN_OUT,
  }
}
export const signIn = (auth) => {
  return {
    type: CONSTANTS.SIGN_IN,
    payload: {
      auth
    }
  }
}
export const signInFailed = (error) => {
  return {
    type: CONSTANTS.SIGN_IN_FAILED,
    payload: {
      error
    }
  }
}
export const signUp = (auth) => {
  return {
    type: CONSTANTS.SIGN_UP,
    payload: {
      auth
    }
  }
}
export const signUpFailed = (error) => {
  return {
    type: CONSTANTS.SIGN_UP_FAILED,
    payload: {
      error
    }
  }
}
export const passwordForget = (email) => {
  return {
    type: CONSTANTS.PASSWORD_FORGET,
    payload: {
      auth: {
        email
      }
    }
  }
}
export const passwordFailed = (error) => {
  return {
    type: CONSTANTS.PASSWORD_FAILED,
    payload: {
      auth: {
        error
      }
    }
  }
}
export const passwordForgetSuccess = () => {
  return {
    type: CONSTANTS.PASSWORD_FORGET_SUCCESS,
    payload: {
      auth: {
        message: 'Send success. Please check your email.'
      }
    }
  }
}
export const passwordChange = (password) => {
  return {
    type: CONSTANTS.PASSWORD_CHANGE,
    payload: {
      password
    }
  }
}
export const passwordChangeSuccess = () => {
  return {
    type: CONSTANTS.PASSWORD_CHANGE_SUCCESS,
    payload: {
      auth: {
        message: 'Change success.'
      }
    }
  }
}