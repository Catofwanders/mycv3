import {CONSTANTS} from '../../constants/actions';

export const adminAddPost = (id,title,descr,date) => {
  return {
    type: CONSTANTS.ADMIN_ADD_POST,
    payload: {id,title,descr,date}
  }
}
export const adminAddPostSuccess = () => {
  return {
    type: CONSTANTS.ADMIN_ADD_POST_SUCCESS,
  }
}
export const adminAddPostFailed = (error) => {
  return {
    type: CONSTANTS.ADMIN_ADD_POST_FAILED,
    payload: {
      error
    }
  }
}
export const adminEditPost = (id,title,descr) => {
  return {
    type: CONSTANTS.ADMIN_EDIT_POST,
    payload: {id,title,descr}
  }
}
export const adminEditPostSuccess = () => {
  return {
    type: CONSTANTS.ADMIN_EDIT_POST_SUCCESS,
  }
}
export const adminEditPostFailed = (error) => {
  return {
    type: CONSTANTS.ADMIN_EDIT_POST_FAILED,
    payload: {
      error
    }
  }
}
export const adminDeletePost = (id) => {
  return {
    type: CONSTANTS.ADMIN_DELETE_POST,
    payload: {
      id
    }
  }
}
export const adminDeletePostSuccess = () => {
  return {
    type: CONSTANTS.ADMIN_DELETE_POST_SUCCESS,
  }
}
export const adminDeletePostFailed = (error) => {
  return {
    type: CONSTANTS.ADMIN_DELETE_POST_FAILED,
    payload: {
      error
    }
  }
}

export const adminGetPosts = () => {
  return {
    type: CONSTANTS.ADMIN_GET_POSTS,
    payload: {
      loading: true
    }
  }
}
export const adminGetPostsSuccess = (posts) => {
  return {
    type: CONSTANTS.ADMIN_GET_POSTS_SUCCESS,
    payload: {
      posts
    }
  }
}
export const adminGetPostsFailed = (error) => {
  return {
    type: CONSTANTS.ADMIN_GET_POSTS_FAILED,
    payload: error 
  }
}
export const adminGetUsers = () => {
  return {
    type: CONSTANTS.ADMIN_GET_USERS,
    payload: {
      loading: true
    }
  }
}
export const adminGetUsersSuccess = (users) => {
  return {
    type: CONSTANTS.ADMIN_GET_USERS_SUCCESS,
    payload: {
      users
    }
  }
}
export const adminGetUsersFailed = (error) => {
  return {
    type: CONSTANTS.ADMIN_GET_USERS_FAILED,
    payload: error 
  }
}
