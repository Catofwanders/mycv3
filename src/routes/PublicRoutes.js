import React, { Suspense, lazy } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import * as ROUTES from '../constants/routes';

const LandingPage = lazy(() => import('../pages/LandingPage'));
const SignUpPage = lazy(() => import('../pages/SignUpPage'));
const SignInPage = lazy(() => import('../pages/SignInPage'));
const PasswordForgetPage = lazy(() => import('../pages/PasswordForgetPage'));
const HomePage = lazy(() => import('../pages/HomePage'));
const AccountPage = lazy(() => import('../pages/AccountPage'));
const AdminPage = lazy(() => import('../pages/AdminPage'));

const PublicRoutes = ({auth}) => {
	return (
		<Suspense fallback={<div />}>
			<Switch>
          <Route 
            exact 
            path={ROUTES.LANDING} 
            render={routeProps => <LandingPage 
            {...routeProps}/>} />
          <Route 
            exact 
            path={ROUTES.SIGN_UP} 
            render={(routeProps) => <SignUpPage 
            {...routeProps}/>} />
          <Route 
            exact 
            path={ROUTES.SIGN_IN} 
            render={(routeProps) => <SignInPage 
            {...routeProps}/>} />
          <Route 
            exact 
            path={ROUTES.PASSWORD_FORGET} 
            render={(routeProps) => {
              return auth 
              ? <Redirect to="/" /> 
              : <PasswordForgetPage {...routeProps} /> } } />
          <Route 
            exact 
            path={ROUTES.HOME} 
            render={(routeProps) => {
              return auth 
              ? <HomePage {...routeProps} auth={auth} />
              : <Redirect to="/" /> } } />
          <Route 
            exact 
            path={ROUTES.ACCOUNT} 
            render={(routeProps) => {
              return auth 
              ? <AccountPage />
              : <Redirect to="/" />} } />
          <Route
            path={ROUTES.ADMIN} 
            render={(routeProps) => {
              return auth
              ? <AdminPage {...routeProps} />
              : <Redirect to="/" />} } />
			</Switch>
		</Suspense>
	);
};

export default PublicRoutes;
export { PublicRoutes };
