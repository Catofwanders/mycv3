import React, { Suspense, lazy } from 'react';
import { Route, Switch } from 'react-router-dom';
import {Spinner} from 'react-bootstrap';
import * as ROUTES from '../constants/routes';

const AdminPosts = lazy(() => import('../pages/AdminPosts'));
const AdminSettings = lazy(() => import('../pages/AdminSettings'));
const AdminUsers = lazy(() => import('../pages/AdminUsers'));

const AdminRoutes = () => {
  
	return (
		<Suspense fallback={<div />}>
			<Switch>
          <Route 
            exact 
            path={ROUTES.ADMIN_POSTS} 
            render={(routeProps) => <AdminPosts {...routeProps} />} />
          <Route 
            exact 
            path={ROUTES.ADMIN_SETTINGS} 
            render={(routeProps) => <AdminSettings {...routeProps} />} />
          <Route 
            exact 
            path={ROUTES.ADMIN_USERS}
            render={(routeProps) => <AdminUsers {...routeProps} />} />
			</Switch>
		</Suspense>
	);
};

export default AdminRoutes;
export { AdminRoutes };
