import { PublicRoutes } from './PublicRoutes';
import { AdminRoutes } from './AdminRoutes';

export {
	PublicRoutes,
	AdminRoutes,
};