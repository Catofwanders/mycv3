import React from 'react';
import * as ROUTES from '../../constants/routes';
import {Nav} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

const AdminNav = () => {
  return (
    <Nav className="flex-column" variant="pills" style={styles.AdminNav}>
        <Nav.Item>
          <LinkContainer to={ROUTES.ADMIN_POSTS}>
            <Nav.Link>Posts</Nav.Link>
          </LinkContainer>
        </Nav.Item>
        <Nav.Item>
          <LinkContainer to={ROUTES.ADMIN_USERS}>
            <Nav.Link>Users</Nav.Link>
          </LinkContainer>
        </Nav.Item>
        <Nav.Item>
          <LinkContainer to={ROUTES.ADMIN_SETTINGS}>
            <Nav.Link>Settings</Nav.Link>
          </LinkContainer>
        </Nav.Item>
    </Nav>
  )
}

const styles = {
  AdminNav: {
    marginTop: 10
  }
}

export default AdminNav;
export {AdminNav};