import React from 'react';
import {useDispatch} from 'react-redux';
import {signOut} from '../../store/actions';
import {Nav, Navbar} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import SignOutButton from '../../components/SignOutButton';
import * as ROUTES from '../../constants/routes';

const Navigation = ({auth}) => {
  const dispatch = useDispatch();
  const NavigationAuth = () => (
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand>CRM</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Item>
          <LinkContainer to={ROUTES.LANDING}>
            <Nav.Link>Landing</Nav.Link>
          </LinkContainer>
        </Nav.Item>
        <Nav.Item>
          <LinkContainer to={ROUTES.HOME}>
            <Nav.Link>Home</Nav.Link>
          </LinkContainer>
        </Nav.Item>
        <Nav.Item>
          <LinkContainer to={ROUTES.ACCOUNT}>
            <Nav.Link>Account</Nav.Link>
          </LinkContainer>
        </Nav.Item>
        <Nav.Item>
          <LinkContainer to={ROUTES.ADMIN_POSTS}>
            <Nav.Link>Admin</Nav.Link>
          </LinkContainer>
        </Nav.Item>
      </Nav>
          <SignOutButton signOut={signOut}/>
    </Navbar>
  );
  
  const NavigationNonAuth = () => (
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand>CRM</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Item>
          <LinkContainer to={ROUTES.LANDING}>
            <Nav.Link>Landing</Nav.Link>
          </LinkContainer>
        </Nav.Item>
        <Nav.Item>
          <LinkContainer to={ROUTES.SIGN_IN}>
            <Nav.Link>Sign In</Nav.Link>
          </LinkContainer>
        </Nav.Item>
      </Nav>
    </Navbar>
  );
  
  return (
    <div style={{marginBottom: '1rem'}}>
      {auth ? <NavigationAuth signOut={() => dispatch(signOut)}/> : <NavigationNonAuth />}
    </div>
  );
}



export default Navigation;
export {Navigation};