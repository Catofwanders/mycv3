import React from 'react';
import { useMount } from 'react-use';
import {useDispatch, useSelector} from 'react-redux';
import {selectIsLoggedIn} from '../../store/selectors/authSelectors';
import Navigation from '../Navigation';
import { PublicRoutes } from '../../routes';
import * as actions from '../../store/actions';
import { Container } from 'react-bootstrap';
import { ConnectedRouter } from 'connected-react-router';

import 'bootstrap/dist/css/bootstrap.min.css';

const App = ({history}) => {

  const dispatch = useDispatch();

  const isLoggedIn = useSelector(selectIsLoggedIn);
  
  useMount(()=>{
    dispatch(actions.setAuth());
    dispatch(actions.getPosts());
    dispatch(actions.adminGetPosts());
    dispatch(actions.adminGetUsers());
  }, []);

  return(
    <Container>
      <ConnectedRouter history={history}>
        <Navigation auth={isLoggedIn} />
        <PublicRoutes auth={isLoggedIn}/>
      </ConnectedRouter>
    </Container>
  )
};

export default App;