import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
};

class FirebaseConstruct {
  constructor() {
    app.initializeApp(config);

    this.auth = app.auth();
    this.db = app.database();
  }

  // *** User API ***

  user = uid => this.db.ref(`authentication/users/${uid}`);

  users = () => this.db.ref('authentication/users');

  // *** Database API ***
  
  post = pid => this.db.ref(`posts/${pid}`);

  posts = () => this.db.ref('posts/');
}

const Firebase = new FirebaseConstruct(); 

export default Firebase;
