import React from 'react';
import {Row, Col, Table} from 'react-bootstrap';
import PostCard from '../PostCard';

function AdminPostsTable({posts}) {
  return (
    <Row>
      <Col>
        <h2>List of the posts</h2>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>id</th>
              <th>title</th>
              <th>description</th>
              <th>created date</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {posts ? posts.map(({uid, post_title, post_descr, post_date}) => (
              <PostCard
                key={uid}
                uid={uid}
                post_title={post_title}
                post_descr={post_descr} 
                post_date={post_date}
              />
            )): null}
          </tbody>
        </Table>
      </Col>
    </Row>
  )
}

export default AdminPostsTable;
export {AdminPostsTable};