import React, {useState} from 'react';
import {Form, Row, Col} from 'react-bootstrap';
import {Input, Button} from '../../lib/ui';
import {useDispatch} from 'react-redux';
import {adminAddPost} from '../../store/actions/adminActions';

function AddPostForm() {
  const dispatch = useDispatch();
  const [title, setTitle] = useState('');
  const [descr, setDescr] = useState('');
  const handleSubmit = event => {
    event.preventDefault();
    const currentDate = new Date();
    const id = currentDate.getTime();
    const date = id;
    dispatch(adminAddPost(id, title, descr, date));
    setTitle('');
    setDescr('');
  };
  const handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    if(name === 'title'){
      setTitle(value);
    }else if(name === 'descr'){
      setDescr(value);
    }
  }
  return (
    <Row style={{marginBottom: '1rem'}}>
      <Col>
        <h2>Create post</h2>
        <Form noValidate onSubmit={handleSubmit}>
          <Input
            type="text"
            name="title"
            value={title}
            onChange={handleChange}
            placeholder="Enter post title"
            />
          <br/>
          <Input
            className="form-control"
            as="textarea"
            name="descr"
            rows="3"
            value={descr}
            onChange={handleChange}
            placeholder="Enter post description"
          />
          <br/>
          <Button type="submit">Submit</Button>
        </Form>
      </Col>
    </Row>
  )
}

export default AddPostForm;
export {AddPostForm};