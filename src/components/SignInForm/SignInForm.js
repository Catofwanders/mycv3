import React, {useState} from 'react';
import {Form} from 'react-bootstrap';
import {Input, Button} from '../../lib/ui';
import { useDispatch } from 'react-redux';
import * as actions from '../../store/actions';

const SignInForm = () => {
  const dispatch = useDispatch();
  const error = '';
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');

  const handleSubmit = event => {
    event.preventDefault();
    const obj = {email, password};
    dispatch(actions.signIn(obj));
  };

  const handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    if(name === 'password'){
      setPassword(value)
    }else if(name === 'email'){
      setEmail(value);
    }
  };

  const isInvalid = password === '' || email === '';

  return (
    <Form onSubmit={handleSubmit} style={styles.formStyle}>
      <Form.Group>
        <Input
          name="email"
          onChange={handleChange}
          type="text"
          placeholder="Email Address"
        />
      </Form.Group>
      <Form.Group>
        <Input
          name="password"
          onChange={handleChange}
          type="password"
          placeholder="Password"
        />
      </Form.Group>
      <Form.Group>
        <Button variant="primary" disabled={isInvalid} type="submit">
          Sign In
        </Button>
      </Form.Group>
      <Form.Group>
        {error ? <Form.Label><p>{error}</p></Form.Label> : null}
      </Form.Group>
    </Form>
  );
}

const styles = {
  formStyle: {
    maxWidth: 400
  }
}

export default SignInForm;
export {SignInForm};