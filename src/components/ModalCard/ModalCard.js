import React from 'react';
import {Modal, Form} from 'react-bootstrap';
import {Input, Button} from '../../lib/ui';

function ModalCard({title, descr, show, close, handleSubmit, handleChange}) {

  return (
    <Modal show={show} onHide={close}>
      <Modal.Header closeButton>
        <Modal.Title>Edit this post</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group>
          <Input
            type="text"
            name="title"
            value={title}
            onChange={handleChange}
            />
        </Form.Group>
        <Form.Group>
          <Input
            as="textarea"
            name="descr"
            rows="3"
            className="form-control"
            value={descr}
            onChange={handleChange}
          />
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={close}>
          Close
        </Button>
        <Button variant="primary" onClick={handleSubmit}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default ModalCard;
export {ModalCard};