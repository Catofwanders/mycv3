import React, { useState } from 'react';
import {Form} from 'react-bootstrap';
import {Input, Button} from '../../lib/ui';
import {useDispatch} from 'react-redux';
import * as actions from '../../store/actions';

function PasswordForgetForm({error, success}) {
  const dispatch = useDispatch();  
  const [email, setEmail] = useState('');

  const handleSubmit = event => {
    event.preventDefault();
    dispatch(actions.passwordForget(email));
    setEmail('');
  };
  const handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    if(name === 'email'){
      setEmail(value);
    }
  }
  const isInvalid = email === '';

  return (
    <Form onSubmit={handleSubmit} style={styles.formStyle}>
      <Form.Group>
        <Input
          name="email"
          value={email}
          onChange={handleChange}
          type="text"
          placeholder="Email Address"
        />
      </Form.Group>
      <Form.Group>
        <Button variant="primary" disabled={isInvalid} type="submit">
          Reset My Password
        </Button>
      </Form.Group>
      <Form.Group>
        {error ? <Form.Label><p>{error}</p></Form.Label> : null}
        {success ? <Form.Label><p>{success}</p></Form.Label> : null}
      </Form.Group>
    </Form>
  );
}

const styles = {
  formStyle: {
    maxWidth: 400
  }
}

export default PasswordForgetForm;
export { PasswordForgetForm };
