import React, {useState} from 'react';
import {Button} from '../../lib/ui';
import ModalCard from '../ModalCard';
import {useDispatch} from 'react-redux';
import {adminDeletePost, adminEditPost} from '../../store/actions/adminActions';

function PostCard({uid, post_title, post_descr, post_date}) {
  const dispatch = useDispatch();
  const [show, setShow] = useState(false);
  const [title, setTitle] = useState('');
  const [descr, setDescr] = useState('');

  const handleClose = () => {
    setShow(false);
  }
  const handleShow = () => {
    setShow(true);
    setTitle(post_title);
    setDescr(post_descr);
  }
  const handleSubmit = event => {
    handleClose();
    dispatch(adminEditPost(uid, title, descr));
  };
  const handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    if(name === 'title'){
      setTitle(value);
    }else if(name === 'descr'){
      setDescr(value);
    }
  }

  const modal = show ?
    <ModalCard 
    show={show}
    close={handleClose}
    uid={uid}
    title={title}
    descr={descr}
    editPost={adminEditPost}
    handleSubmit={handleSubmit}
    handleChange={handleChange}
  />
  : null;
  return (
    <tr>
      <td>{uid}</td>
      <td>{post_title}</td>
      <td>{post_descr}</td>
      <td>{new Date(post_date).toDateString()}</td>
      <td><Button type="submit" variant="primary" onClick={handleShow}>Edit</Button></td>
      <td><Button variant="secondary" onClick={() => dispatch(adminDeletePost(uid))}>Delete</Button></td>
      {modal}
    </tr>
  )
}

export default PostCard;
export {PostCard};