import React from 'react';
import {Button} from '../../lib/ui';
import { useDispatch } from 'react-redux';
import { signOut } from '../../store/actions/authActions';

const SignOutButton = () => {

  const dispatch = useDispatch();
  
  const onSignOut = () => {
    dispatch(signOut());
  };

  return (
    <Button variant="primary"  onClick={onSignOut}>
      Sign Out
    </Button>
  );
}


export default SignOutButton;
export {SignOutButton};