import React, {useState} from 'react';
import {Form, Col} from 'react-bootstrap';
import {Input, Button} from '../../lib/ui';
import { useDispatch } from 'react-redux';
import {signUp} from '../../store/actions';

const SignUpForm = () => {
  const dispatch = useDispatch();
  const error = '';
  const [email, setEmail] = useState('');
  const [passwordOne, setPasswordOne] = useState('');
  const [passwordTwo, setPasswordTwo] = useState('');
  const [username, setUsername] = useState('');

  const handleSubmit = event => {
    event.preventDefault();
    const obj = {email, passwordOne, username};
    dispatch(signUp(obj));
  };
  const handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    if(name === 'passwordOne'){
      setPasswordOne(value)
    }else if(name === 'passwordTwo'){
      setPasswordTwo(value)
    }else if(name === 'email'){
      setEmail(value);
    }else if(name === 'username'){
      setUsername(value);
    }
  };
  const isInvalid =
    passwordOne !== passwordTwo ||
    passwordOne === '' ||
    email === '' ||
    username === '';

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Row>
        <Form.Group as={Col}>
          <Input
            name="username"
            onChange={handleChange}
            type="text"
            placeholder="Full Name"
            />
        </Form.Group>
        <Form.Group as={Col}>
          <Input
            name="email"
            onChange={handleChange}
            type="text"
            placeholder="Email Address"
            />
        </Form.Group>
      </Form.Row>
      <Form.Row>
        <Form.Group as={Col}>
          <Input
            name="passwordOne"
            onChange={handleChange}
            type="password"
            placeholder="Password"
          />
        </Form.Group>
        <Form.Group as={Col}>
          <Input
            name="passwordTwo"
            onChange={handleChange}
            type="password"
            placeholder="Confirm Password"
          />
        </Form.Group>
      </Form.Row>
      <Form.Group>
        <Button variant="primary" disabled={isInvalid} type="submit">
          Sign Up
        </Button>
      </Form.Group>
      <Form.Group>
        {error ? <Form.Label><p>{error}</p></Form.Label> : null}
      </Form.Group>
    </Form>
  );
}

export default SignUpForm;
export {SignUpForm};