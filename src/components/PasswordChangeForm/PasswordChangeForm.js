import React, { useState } from 'react';
import {useDispatch} from 'react-redux';
import * as actions from '../../store/actions';
import {Form} from 'react-bootstrap';
import {Input, Button} from '../../lib/ui';

function PasswordChangeForm({error, success}) {
  const dispatch = useDispatch();  
  const [passwordOne, setPasswordOne] = useState('');
  const [passwordTwo, setPasswordTwo] = useState('');
  
  const handleSubmit = event => {
    event.preventDefault();
    dispatch(actions.passwordChange(passwordOne));
  };
  
  const handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    if(name === 'passwordOne'){
      setPasswordOne(value);
    }else if(name === 'passwordTwo'){
      setPasswordTwo(value);
    }
  }
  const isInvalid = passwordOne !== passwordTwo || passwordOne === '';
  return (
    <Form onSubmit={handleSubmit} style={styles.formStyle}>
      <Form.Group>
        <Input
          name="passwordOne"
          value={passwordOne}
          onChange={handleChange}
          type="password"
          placeholder="New Password"
        />

      </Form.Group>
      <Form.Group>
        <Input
          name="passwordTwo"
          value={passwordTwo}
          onChange={handleChange}
          type="password"
          placeholder="Confirm New Password"
        />
      </Form.Group>
      <Form.Group>
        <Button variant="primary" disabled={isInvalid} type="submit">
          Change My Password
        </Button>
      </Form.Group>
      <Form.Group>
        {error ? <Form.Label><p>{error}</p></Form.Label> : null}
        {success ? <Form.Label><p>{success}</p></Form.Label> : null}
      </Form.Group>
    </Form>
  );
}

const styles = {
  formStyle: {
    maxWidth: 400
  }
}

export default PasswordChangeForm;
export {PasswordChangeForm};