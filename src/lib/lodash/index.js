import cloneDeep from 'lodash/cloneDeep';
import isArray from 'lodash/isArray';
import isEqual from 'lodash/isEqual';

export {
	cloneDeep,
	isArray,
	isEqual,
};
