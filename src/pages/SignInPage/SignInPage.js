import React from 'react';
import { PasswordForgetLink } from '../../components/PasswordForgetLink';
import SignInForm from '../../components/SignInForm';
import SignUpLink from '../../components/SignUpLink';

const SignInPage = () => (
  <div>
    <h1>SignIn</h1>
    <SignInForm/>
    <PasswordForgetLink />
    <SignUpLink /> 
  </div>
);

export default SignInPage;
export { SignInPage };
