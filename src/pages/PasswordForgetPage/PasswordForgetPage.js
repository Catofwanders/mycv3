import React from 'react';
import {PasswordForgetForm} from '../../components/PasswordForgetForm'; 
import {useSelector} from 'react-redux';
import {selectUser} from "../../store/selectors/authSelectors";

const PasswordForgetPage = () => {

  const user = useSelector(selectUser);
  
  return(
    <div>
      <h1>PasswordForget</h1>
      <PasswordForgetForm error={user.error} success={user.message}/>
    </div>
  );
}

export default PasswordForgetPage;
export {PasswordForgetPage};