import React from 'react';
import AddPostForm from '../../components/AddPostForm';
import AdminPostsTable from '../../components/AdminPostsTable';
import {useSelector} from 'react-redux';
import {selectPosts} from '../../store/selectors/adminSelectors';
import * as PropTypes from 'prop-types';

const AdminPosts = () => {
  const posts = useSelector(selectPosts);
  
  return (
    <div>
      <AddPostForm/>
      <AdminPostsTable
        posts={posts}
        />
    </div>
  )
}

AdminPosts.propTypes = {
  posts: PropTypes.array
}

export default AdminPosts;
export {AdminPosts};