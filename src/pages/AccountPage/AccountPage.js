import React from 'react';
import PasswordChangeForm from '../../components/PasswordChangeForm';
import {useSelector} from 'react-redux';
import {selectUser} from "../../store/selectors/authSelectors";

const AccountPage = () => {
  const user = useSelector(selectUser);
  return(
    <div>
      <h1>Account: {user.email}</h1>
      <h2>Change your password</h2>
      <PasswordChangeForm error={user.error} success={user.message}/>
    </div>
  )
};

export default AccountPage;
export {AccountPage};