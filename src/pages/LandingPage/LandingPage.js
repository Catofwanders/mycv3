import React from 'react';
import {Card, Button, Row, Col, Spinner} from 'react-bootstrap';
import {selectPosts} from '../../store/selectors/postsSelectors';
import {useSelector} from 'react-redux';

const Landing = () => {
  
  const posts = useSelector(selectPosts);
  const postsList = posts ? posts.map(({uid, post_title, post_descr, post_date}) => (
    <Col key={uid} sm={4}>
      <Card className="text-center mb-4">
        <Card.Body>
          <Card.Title>{post_title}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">{new Date(post_date).toDateString()}</Card.Subtitle>
          <Card.Text>
            {post_descr}
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
  </Col>
  )) : <Spinner className="text-center" animation="grow" />;

  return(
    <div>
      <h1 className="text-center">Landing</h1>
      <h2 className="text-center">Recent posts</h2>
      <Row>
        {postsList}
      </Row>
    </div>
  )
};

export default Landing;
export {Landing};