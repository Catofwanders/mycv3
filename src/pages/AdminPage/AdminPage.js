import React from 'react';
import AdminNav from '../../containers/AdminNav';
import {AdminRoutes} from '../../routes';
import {Row, Col} from 'react-bootstrap';


const AdminPage = () => {
  return (
    <Row>
      <Col sm={3}>
        <AdminNav />
      </Col>
      <Col sm={9}>
        <AdminRoutes/>
      </Col>
    </Row>
  );
}

export default AdminPage;