import React from 'react';
import {Table} from 'react-bootstrap';
import {useSelector} from 'react-redux';
import {selectUsers} from '../../store/selectors/adminSelectors';

const AdminUsers = () => {

  const users = useSelector(selectUsers);
  const content = users ?
  <Table striped bordered>
    <thead>
      <tr>
        <td><strong>ID:</strong></td>
        <td><strong>E-Mail:</strong></td>
        <td><strong>Username:</strong></td>
      </tr>
    </thead>
    <tbody>
      {users.map(user => (
        <tr key={user.uid}>
          <td>
            {user.uid}
          </td>
          <td>
            {user.email}
          </td>
          <td>
            {user.username}
          </td>
        </tr>
      ))}
    </tbody>
  </Table>
  : <p>Loading...</p>;

  return content;
}

export default AdminUsers;
export {AdminUsers};